﻿using Microsoft.Extensions.DependencyInjection;
using Sabis.Birimler;
using System;
using System.Diagnostics;

namespace Sabis.EventBus.ConsoleTest
{
    class Program
    {
        static void Main(string[] args)
        {
            IServiceCollection services = new ServiceCollection();
            services.AddBirimlerService();

            IServiceProvider serviceProvider = services.BuildServiceProvider();
            using (IServiceScope scope = serviceProvider.CreateScope())
            {
                IBirimlerService _birimler = scope.ServiceProvider.GetRequiredService<IBirimlerService>();
                var timer = Stopwatch.StartNew();
                var result = _birimler.getirBirimbyID(255);
                Console.WriteLine(result.ID);
                Console.WriteLine(result.BirimAdi);
                Console.WriteLine(result.BirimUzunAdi);
                Console.WriteLine(result.EnumBirimTuru);
                var result2 = _birimler.getirUstBirim(result.ID);
                var result3 = _birimler.getirAltBirimler(255);

                timer.Stop();
                var ElapsedTimeMilliseconds = timer.ElapsedMilliseconds;
                Console.WriteLine(ElapsedTimeMilliseconds);
                timer = Stopwatch.StartNew();
                _birimler.RefreshBirimler();
                timer.Stop();
                var elapsed = timer.ElapsedMilliseconds;

                Console.WriteLine(elapsed);

            }
            Console.ReadLine();
        }
    }
}
