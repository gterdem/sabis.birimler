﻿using System.ComponentModel;

namespace Sabis.Birimler
{
    public enum EnumDil
    {
        [Description("Türkçe")]
        TURKCE = 0,
        [Description("İngilizce")]
        INGILIZCE = 1,
        [Description("Almanca")]
        ALMANCA = 2,
        [Description("Rusça")]
        RUSCA = 3,
        [Description("Japonca")]
        JAPONCA = 4,
        [Description("Çince")]
        CINCE = 5,
        [Description("Arapça")]
        ARAPCA = 6,
        [Description("Fransızca")]
        FRANSIZCA = 7,
        [Description("İspanyolca")]
        Ispanyolca = 8,
        [Description("İtalyanca")]
        Italyanca = 9,
    }
    public enum EnumOgretimTur
    {
        [Description("Belirsiz")]
        BELIRSIZ = 0,
        [Description("1. Öğretim")]
        OGRETIM_1 = 1,
        [Description("2. Öğretim")]
        OGRETIM_2 = 2,
        [Description("Uzaktan Eğitim")]
        UZAKTAN = 3
    }
    public enum EnumBirimTuru
    {
        [Description("BELIRSIZ")]
        Belirsiz = -1,
        [Description("Lisans Programı")]
        LisansProgrami = 104,
        [Description("ÖnLisans Programı")]
        OnLisansProgrami = 105,
        [Description("Yabancı Dil Hazırlık Okulu")]
        YDilHazirlikOkulu = 106,
        [Description("Tezsiz Yüksek Lisans Programı")]
        TezsizYLisansProg = 107,
        [Description("Bilimsel Hazırlık Programı")]
        BilimselHazirlikProg = 108,
        [Description("Yabacı Dil Hazırlık Programı")]
        YDilHazirlikProg = 109,
        [Description("Kurul")]
        Kurul = 110,
        [Description("Komisyon")]
        Komisyon = 111,
        [Description("DGS Lisans Programı")]
        DGSLisansProg = 112,
        [Description("CAP Lisans Programı")]
        CAPLisansProg = 113,
        [Description("Yandal Lisans Programı")]
        YandalLisansProg = 114,
        [Description("Üniversite")]
        Universite = 0,
        [Description("Rektörlük")]
        Rektorluk = 1,
        [Description("Fakülte")]
        Fakulte = 2,
        [Description("Bölüm")]
        Bolum = 3,
        [Description("Program")]
        Program = 4,
        [Description("Ana Bilim Dalı")]
        Anabilimdali = 5,
        [Description("Bilim Dalı")]
        BilimDali = 6,
        [Description("Yüksekokul")]
        YuksekOkul = 7,
        [Description("Meslek Yüksekokulu")]
        MeslekYuksekOkulu = 8,
        [Description("Enstitü")]
        Enstitu = 9,
        [Description("Araştırma Merkezi")]
        ArastirmaMerkezi = 10,
        [Description("Bölüm Başkanlığı")]
        BolumBaskanligi = 11,
        [Description("Ana Sanat Dalı")]
        AnaSanatDali = 12,
        [Description("Yüksek Lisans Anabilim Dalı")]
        YuksekLisansAnaBilimDali = 13,
        [Description("Doktora Anabilim Dalı")]
        DoktoraAnaBilimDali = 14,
        [Description("Sanatta Yeterlilik Anabilim Dalı")]
        SanattaYeterlilikAnaBilimDali = 15,
        [Description("Vakıf Meslek Yüksek Okulu")]
        VakifMeslekYuksekOkulu = 16,
        [Description("Tezli Yüksek Lisans Programı")]
        YuksekLisansProgrami = 17,
        [Description("Doktora Programı")]
        DoktoraProgrami = 18,
        [Description("Sanat Dalı")]
        SanatDali = 20,
        [Description("Sanatta Yeterlilik Programı")]
        SanattaYeterlilikProgrami = 21,
        /// <summary>
        /// Kaldırılacak ve Program bunun yerine kullanılacak.
        /// </summary>
        [Description("Ara Program")]
        AraProgram = 23,
        [Description("Genel Sekreterlik")]
        GenelSekreterlik = 50,
        [Description("Daire Başkanlığı")]
        DaireBaskanligi = 51,
        [Description("Şube Müdürlüğü")]
        SubeMudurlugu = 52,
        [Description("Müdürlük")]
        Mudurluk = 53,
        [Description("Koordinatörlük")]
        Koordinatorluk = 54,
        [Description("Uzmanlık")]
        Uzmanlik = 55,
        [Description("Müşavirlik")]
        Musavirlik = 56,
        [Description("Özel Kalem")]
        OzelKalem = 57,
        [Description("Sosyal Hizmet")]
        SosyalHizmet = 58,
        [Description("Öğrenci Topluluğu")]
        OgrenciToplulugu = 200,
        [Description("Laboratuvar")]
        Laboratuvar = 201,
    }
}
