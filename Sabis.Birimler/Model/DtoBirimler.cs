﻿using Sabis.Common.Enum;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Sabis.Birimler
{
    public class DtoBirimler
    {
        public DtoBirimler()
        {
            GecerliDil = EnumDil.TURKCE;
        }
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public Nullable<long> YoksisID { get; set; }
        [DataMember]
        public string BirimAdi { get; set; }
        [DataMember]
        public string BirimAdiIngilizce { get; set; }
        [DataMember]
        public Nullable<int> UstBirimID { get; set; }
        [DataMember]
        public bool Aktif { get; set; }
        [DataMember]
        public int EnumAktiflikStatusu { get; set; }
        [DataMember]
        public string BirimTuruAdi { get; set; }
        [DataMember]
        public string BirimUzunAdi { get; set; }
        [DataMember]
        public Nullable<int> EnumBirimTuru { get; set; }
        [DataMember]
        public Nullable<int> IlKodu { get; set; }
        [DataMember]
        public Nullable<int> IlceKodu { get; set; }
        [DataMember]
        public string OgrenimDili { get; set; }
        [DataMember]
        public Nullable<int> OgrenimSuresi { get; set; }
        [DataMember]
        public string OgrenimTuru { get; set; }
        [DataMember]
        public Nullable<int> EnumOgrenimTuru { get; set; }
        [DataMember]
        public string AcikAdres { get; set; }
        [DataMember]
        public Nullable<long> KlavuzKodu { get; set; }
        [DataMember]
        public Nullable<int> EnumKategori { get; set; }
        [DataMember]
        public Nullable<int> FKUniversiteID { get; set; }
        [DataMember]
        public Nullable<int> FKUlkeID { get; set; }
        [DataMember]
        public Nullable<int> FKYonlendirilenPrID { get; set; }
        [DataMember]
        public Nullable<int> UlkeKod { get; set; }
        [DataMember]
        public Nullable<int> UniversiteKod { get; set; }
        [DataMember]
        public Nullable<int> FakulteKod { get; set; }
        [DataMember]
        public Nullable<int> BolumKod { get; set; }
        [DataMember]
        public Nullable<int> AnaBilimDaliKod { get; set; }
        [DataMember]
        public string Ogr_FakulteKod { get; set; }
        [DataMember]
        public string Ogr_BolumKod { get; set; }
        [DataMember]
        public Nullable<int> InKod { get; set; }
        [DataMember]
        public Nullable<int> Sene { get; set; }
        [DataMember]
        public string AdGerman { get; set; }
        [DataMember]
        public Nullable<int> AzamiSure { get; set; }
        [DataMember]
        public Nullable<bool> DerslereDevamMecburiyeti { get; set; }
        [DataMember]
        public string EgitimEsasi { get; set; }
        [DataMember]
        public string DekanMakam { get; set; }
        [DataMember]
        public string DekanMakamEng { get; set; }
        [DataMember]
        public string BasilanBirimAdi { get; set; }
        [DataMember]
        public string BasilanBirimAdiEng { get; set; }
        [DataMember]
        public Nullable<int> EnumSeviye { get; set; }
        [DataMember]
        public string BirimKodu { get; set; }
        [DataMember]
        public Nullable<System.Guid> GrupGUID { get; set; }
        [DataMember]
        public string BasilanBirimAdiKisa { get; set; }
        [DataMember]
        public string BasilanBirimAdiKisaEng { get; set; }
        [DataMember]
        public Nullable<int> EnstituFakulteIliskiID { get; set; }
        [DataMember]
        public int VersiyonBaslangicYil { get; set; }
        [DataMember]
        public Nullable<int> VersiyonBitisYil { get; set; }
        [DataMember]
        public Nullable<int> FKOncekiVersiyonID { get; set; }
        [DataMember]
        public Nullable<int> OgretimTurleri { get; set; }
        [DataMember]
        public System.DateTime TarihKayit { get; set; }
        [DataMember]
        public Nullable<System.DateTime> TarihGuncelleme { get; set; }
        [DataMember]
        public Nullable<int> EnumDersSureTip { get; set; }
        [DataMember]
        public Nullable<int> EnumTabanNotTipi { get; set; }
        [DataMember]
        public bool Silindi { get; set; }
        [DataMember]
        public List<DtoBirimDil> BirimDilList { get; set; }
        [DataMember]
        public EnumDil GecerliDil { get; set; }
    }
}
