﻿using Sabis.Common.Enum;
using System.Runtime.Serialization;

namespace Sabis.Birimler
{
    public class DtoBirimDil
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public int FKBirimID { get; set; }
        [DataMember]
        public EnumDil EnumDil { get; set; }
        [DataMember]
        public string BirimAdi { get; set; }
        [DataMember]
        public string BasilanBirimAdi { get; set; }
        [DataMember]
        public string BasilanBirimAdiKisa { get; set; }
    }
}
