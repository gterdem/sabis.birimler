﻿using Microsoft.Extensions.DependencyInjection;

namespace Sabis.Birimler.DependencyInjection
{
    public interface IBirimlerBuilder
    {
        IServiceCollection Services { get; }
        IBirimlerBuilder UseConfig(string birimUrl, int universityId);
        IBirimlerBuilder UseSabisConfig();
    }
}
