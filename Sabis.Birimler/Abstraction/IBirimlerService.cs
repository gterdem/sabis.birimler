﻿using Sabis.Common.Enum;
using System.Collections.Generic;

namespace Sabis.Birimler
{
    /// <summary>
    /// Sabis Birimler Servis
    /// </summary>
    public interface IBirimlerService
    {
        Dictionary<int, DtoBirimler> getirTumBirimler();
        List<DtoBirimler> getirAltBirimListe(int birimID);
        List<DtoBirimler> getirBirimListe(List<int> birimIDList, bool? birAltBirimleriGetir = false, bool? tumAltBirimleriGetir = false);
        DtoBirimler getirBirimbyID(int birimID, bool ifNullReturnNewObject = false);
        List<DtoBirimler> getirBirimbyYoksisID(long YoksisID);
        bool isFakulte(int brmID);
        int? getirBagliFakulteID(int brmID);
        int? getirBagliBolumID(int brmID);
        DtoBirimler getirBagliFakulte(int brmID, bool ifNullReturnNewObject = false);
        DtoBirimler getirBagliBolum(int brmID, bool ifNullReturnNewObject = false);
        DtoBirimler getirBagliAraProgram(int brmID, bool ifNullReturnNewObject = false, bool yoksaKendiniGetir = true);
        DtoBirimler getirUstBirim(int brmID);
        List<DtoBirimler> getirUstBirimList(int brmID);
        List<int> getirUstBirimIDList(int brmID);
        List<DtoBirimler> getirAltBirimler(int birimID, bool kendisiDahilMi = false, bool kapaliBirimlerGelsinMi = false, List<int> birimTipleri = null, int? ogretimTuru = null, List<int> aktiflikStatuleri = null, List<int> EnumOgretimSeviye = null);
        List<int> getirAltBirimlerID(int? brmID, bool kendisiDahilMi = false, bool kapaliBirimlerGelsinMi = false, List<int> birimTipleri = null, int? ogretimTuru = null, List<int> aktiflikStatuleri = null, List<int> EnumOgretimSeviye = null);
        DtoBirimler getirProgramDigerOgretimTuru(int birimID, EnumOgretimTur ogretimTuru);
        List<DtoBirimler> getirProgramDigerOgretimTuruList(int birimID);
        void RefreshBirimler();

    }
}
