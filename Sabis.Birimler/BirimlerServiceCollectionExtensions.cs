﻿using Sabis.Birimler.DependencyInjection;
using System;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class BirimlerServiceCollectionExtensions
    {
        public static IServiceCollection AddBirimlerService(this IServiceCollection services, string birimServiceUrl, int universityId = 1426)
        {
            if (services == null)
            {
                throw new ArgumentNullException(nameof(services));
            }
            IBirimlerBuilder builder = new BirimlerBuilder(services)
                .UseConfig(birimServiceUrl, universityId);

            return services;
        }
        public static IServiceCollection AddBirimlerService(this IServiceCollection services)
        {
            if (services == null)
            {
                throw new ArgumentNullException(nameof(services));
            }
            IBirimlerBuilder builder = new BirimlerBuilder(services)
                .UseSabisConfig();

            return services;
        }
    }
}
