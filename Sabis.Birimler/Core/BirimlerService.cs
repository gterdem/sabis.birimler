﻿using Newtonsoft.Json;
using Sabis.Common.Shared;
using Sabis.Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Sabis.Birimler
{
    public class BirimlerService : IBirimlerService
    {
        private Dictionary<int, DtoBirimler> BirimDict;
        private Dictionary<Guid, List<DtoBirimler>> BirimGuidDict;
        private string _serviceUrl { get; set; }
        private int _universityId { get; set; }
        public BirimlerService()
        {
            if (BirimDict == null)
                BirimDict = CreateBirimDict();
            if (BirimGuidDict == null)
                BirimGuidDict = CreateBirimGuidDict();
        }
        public BirimlerService(string serviceUrl, int universityId)
        {
            if (BirimDict == null)
                BirimDict = CreateBirimDict(serviceUrl, universityId);
            if (BirimGuidDict == null)
                BirimGuidDict = CreateBirimGuidDict();
        }
        public void RefreshBirimler()
        {
            BirimDict = CreateBirimDict(_serviceUrl, _universityId);
        }

        internal Dictionary<int, DtoBirimler> CreateBirimDict(string serviceUrlString = "http://apiortak2.sabis.sakarya.edu.tr/", int universityId = 1426)
        {
            _serviceUrl = serviceUrlString;
            _universityId = universityId;
            Uri serviceUrl = new Uri(serviceUrlString);
            return GetFromHttp(serviceUrl, universityId);
        }

        internal Dictionary<Guid, List<DtoBirimler>> CreateBirimGuidDict()
        {
            Dictionary<Guid, List<DtoBirimler>> result = new Dictionary<Guid, List<DtoBirimler>>();

            var grupList = BirimDict.Where(x => x.Value.GrupGUID.HasValue).GroupBy(x => x.Value.GrupGUID.Value).ToDictionary(x => x.Key, y => y.Select(f => f.Value.ID).ToList());
            foreach (var item in grupList)
            {
                List<DtoBirimler> birimlerTMP = new List<DtoBirimler>();
                foreach (var birimID in item.Value)
                {
                    birimlerTMP.Add(BirimDict[birimID]);
                }
                result.Add(item.Key, birimlerTMP);
            }
            return result;
        }

        public Dictionary<int, DtoBirimler> getirTumBirimler()
        {
            return this.BirimDict;
        }

        public List<DtoBirimler> getirAltBirimListe(int birimID)
        {
            List<DtoBirimler> list = new List<DtoBirimler>();

            foreach (var item in BirimDict.Values)
            {
                if (item.UstBirimID == birimID)
                    list.Add(item);
            }
            return list;
        }

        public List<DtoBirimler> getirAltBirimler(int birimID, bool kendisiDahilMi = false, bool kapaliBirimlerGelsinMi = false, List<int> birimTipleri = null, int? ogretimTuru = null, List<int> aktiflikStatuleri = null, List<int> EnumOgretimSeviye = null)
        {
            List<DtoBirimler> tumBirimler = new List<DtoBirimler>();

            if (BirimDict.Values.Any(m => m.UstBirimID == m.ID))
            {
                throw new System.Exception("UstBirimID  si ID ye Eşit olan Birim Var. Düzeltilmeli!.");
            }

            if (kendisiDahilMi)
            {
                tumBirimler.Add(BirimDict[birimID]);
            }

            tumBirimler.AddRange(RecursiveAltBirimGetir(birimID));
            if (birimTipleri != null)
            {
                tumBirimler = tumBirimler.Where(x => x.EnumBirimTuru.HasValue && birimTipleri.Contains(x.EnumBirimTuru.Value)).ToList();
            }
            if (ogretimTuru != null)
            {
                tumBirimler = tumBirimler.Where(x => x.EnumOgrenimTuru.HasValue && x.EnumOgrenimTuru == ogretimTuru).ToList();
            }
            if (aktiflikStatuleri != null)
            {
                tumBirimler = tumBirimler.Where(x => aktiflikStatuleri.Contains(x.EnumAktiflikStatusu)).ToList();
            }
            if (EnumOgretimSeviye != null && EnumOgretimSeviye.Count > 0)
            {
                tumBirimler = tumBirimler.Where(x => x.EnumSeviye.HasValue && EnumOgretimSeviye.Contains(x.EnumSeviye.Value)).ToList();
            }

            if (kapaliBirimlerGelsinMi)
                return tumBirimler;
            else
                return tumBirimler.Where(x => x.Aktif == true).ToList();
        }

        public List<int> getirAltBirimlerID(int? brmID, bool kendisiDahilMi = false, bool kapaliBirimlerGelsinMi = false, List<int> birimTipleri = null, int? ogretimTuru = null, List<int> aktiflikStatuleri = null, List<int> EnumOgretimSeviye = null)
        {
            List<int> retIDList = new List<int>();

            List<DtoBirimler> retList = getirAltBirimler(brmID.Value, kendisiDahilMi, kapaliBirimlerGelsinMi, birimTipleri, ogretimTuru, aktiflikStatuleri, EnumOgretimSeviye);

            foreach (var item in retList)
                retIDList.Add(item.ID);

            return retIDList;
        }

        public DtoBirimler getirBagliAraProgram(int brmID, bool ifNullReturnNewObject = false, bool yoksaKendiniGetir = true)
        {
            DtoBirimler result = null;
            DtoBirimler tempBirim = BirimDict[brmID];

            if (tempBirim.EnumBirimTuru == (int)EnumBirimTuru.AraProgram)
            {
                result = tempBirim;
            }
            else if (
                (
                tempBirim.EnumBirimTuru == (int)EnumBirimTuru.Program ||
                tempBirim.EnumBirimTuru == (int)EnumBirimTuru.OnLisansProgrami ||
                tempBirim.EnumBirimTuru == (int)EnumBirimTuru.LisansProgrami ||
                tempBirim.EnumBirimTuru == (int)EnumBirimTuru.YuksekLisansProgrami ||
                tempBirim.EnumBirimTuru == (int)EnumBirimTuru.DoktoraProgrami ||
                 tempBirim.EnumBirimTuru == (int)EnumBirimTuru.SanattaYeterlilikProgrami
                ) &&
                tempBirim.UstBirimID.HasValue)
            {
                DtoBirimler ustBirim = BirimDict[tempBirim.UstBirimID.Value];

                if (ustBirim.EnumBirimTuru == (int)EnumBirimTuru.AraProgram)
                {
                    result = ustBirim;
                }
                else if (yoksaKendiniGetir)
                {
                    result = tempBirim;
                }
            }

            if (result == null && ifNullReturnNewObject)
                return new DtoBirimler();

            return result;
        }

        public DtoBirimler getirBagliBolum(int brmID, bool ifNullReturnNewObject = false)
        {
            DtoBirimler result = null;
            DtoBirimler tempBirim = BirimDict[brmID];

            while (result == null)
            {
                if (tempBirim.EnumBirimTuru == (int)EnumBirimTuru.Bolum || tempBirim.EnumBirimTuru == (int)EnumBirimTuru.Anabilimdali || tempBirim.EnumBirimTuru == (int)EnumBirimTuru.AnaSanatDali || tempBirim.EnumBirimTuru == (int)EnumBirimTuru.SanattaYeterlilikAnaBilimDali)
                {
                    result = tempBirim;
                    break;
                }

                if (tempBirim.UstBirimID.HasValue)
                {
                    tempBirim = BirimDict[tempBirim.UstBirimID.Value];
                }
                else
                {
                    break;
                }
            }

            if (result == null && ifNullReturnNewObject)
                return new DtoBirimler();

            return result;
        }

        public int? getirBagliBolumID(int brmID)
        {
            DtoBirimler bol = getirBagliBolum(brmID);

            if (bol is null)
                return null;
            return bol.ID;
        }

        public DtoBirimler getirBagliFakulte(int brmID, bool ifNullReturnNewObject = false)
        {
            DtoBirimler result = null;
            DtoBirimler tempBirim = BirimDict[brmID];

            while (result == null)
            {
                if (isFakulte(tempBirim.ID))
                {
                    result = tempBirim;
                    break;
                }
                if (tempBirim.UstBirimID.HasValue)
                {
                    tempBirim = BirimDict[tempBirim.UstBirimID.Value];
                }
                else
                {
                    break;
                }
            }

            if (result == null && ifNullReturnNewObject)
                return new DtoBirimler();
            return result;
        }
        public int? getirBagliFakulteID(int brmID)
        {
            DtoBirimler fak = getirBagliFakulte(brmID);
            if (fak is null)
                return null;

            return fak.ID;
        }

        public DtoBirimler getirBirimbyID(int birimID, bool ifNullReturnNewObject = false)
        {
            DtoBirimler birim = null;
            if (BirimDict.ContainsKey(birimID))
                birim = BirimDict[birimID];

            if (birim == null && ifNullReturnNewObject)
                birim = new DtoBirimler();

            return birim;
        }

        public List<DtoBirimler> getirBirimbyYoksisID(long YoksisID)
        {
            return BirimDict.Values.Where(m => m.YoksisID == YoksisID).ToList();
        }

        public List<DtoBirimler> getirBirimListe(List<int> birimIDList, bool? birAltBirimleriGetir = false, bool? tumAltBirimleriGetir = false)
        {
            List<DtoBirimler> list = new List<DtoBirimler>();

            foreach (var birimID in birimIDList)
            {
                if (BirimDict.ContainsKey(birimID))
                {
                    list.Add(BirimDict[birimID]);
                    if (tumAltBirimleriGetir == true)
                    {
                        var altBirimler = getirAltBirimler(birimID, false, false);
                        if (altBirimler.Count > 0)
                            list.AddRange(altBirimler);
                    }
                    else if (birAltBirimleriGetir == true)
                    {
                        var birAltBirimler = getirAltBirimListe(birimID);
                        if (birAltBirimler.Count > 0)
                            list.AddRange(birAltBirimler);
                    }
                }
            }
            return list;
        }

        public DtoBirimler getirProgramDigerOgretimTuru(int birimID, EnumOgretimTur ogretimTuru)
        {
            DtoBirimler result = null;

            var birim = BirimDict[birimID];
            if (birim.GrupGUID.HasValue)
            {
                var digerBirimler = BirimGuidDict[birim.GrupGUID.Value];
                result = digerBirimler.FirstOrDefault(x => x.EnumOgrenimTuru == (int)ogretimTuru);
            }
            return result;
        }

        public List<DtoBirimler> getirProgramDigerOgretimTuruList(int birimID)
        {
            List<DtoBirimler> result = null;

            var birim = BirimDict[birimID];
            if (birim.GrupGUID.HasValue)
            {
                result = BirimGuidDict[birim.GrupGUID.Value];
            }
            return result;
        }

        public DtoBirimler getirUstBirim(int brmID)
        {
            DtoBirimler result = null;
            DtoBirimler birim = BirimDict[brmID];

            if (birim.UstBirimID.HasValue)
            {
                result = BirimDict[(int)birim.UstBirimID];
            }
            return result;
        }

        public List<int> getirUstBirimIDList(int brmID)
        {
            List<int> result = new List<int>();
            DtoBirimler birim = BirimDict[brmID];

            while (birim.UstBirimID.HasValue)
            {
                birim = getirUstBirim(birim.ID);
                result.Add(birim.ID);
            }
            return result;
        }

        public List<DtoBirimler> getirUstBirimList(int brmID)
        {
            List<DtoBirimler> result = new List<DtoBirimler>();
            DtoBirimler birim = BirimDict[brmID];

            while (birim.UstBirimID.HasValue)
            {
                birim = getirUstBirim(birim.ID);
                result.Add(birim);
            }
            return result;
        }

        public bool isFakulte(int brmID)
        {
            DtoBirimler birim = BirimDict[brmID];

            if (birim.EnumBirimTuru == (int)EnumBirimTuru.Fakulte || birim.EnumBirimTuru == (int)EnumBirimTuru.Enstitu ||
                birim.EnumBirimTuru == (int)EnumBirimTuru.MeslekYuksekOkulu || birim.EnumBirimTuru == (int)EnumBirimTuru.YuksekOkul)
                return true;

            return false;
        }

        private List<DtoBirimler> RecursiveAltBirimGetir(int birimID, List<DtoBirimler> tumBirimler = null)
        {
            if (tumBirimler == null)
                tumBirimler = new List<DtoBirimler>();

            List<DtoBirimler> tempList = BirimDict.Values.Where(m => m.UstBirimID == birimID).ToList();


            if (tempList != null && tempList.Count > 0)
            {
                tumBirimler.AddRange(tempList);
                foreach (var birim in tempList)
                {
                    RecursiveAltBirimGetir(birim.ID, tumBirimler);
                }
            }
            else
            {
                return tumBirimler;
            }

            return tumBirimler;
        }

        private Dictionary<int, DtoBirimler> GetFromHttp(Uri serviceUrl, int univId)
        {
            Dictionary<int, DtoBirimler> result = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = serviceUrl;
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = client.GetAsync("api/Birimler/GetirBirimDictv3?univID=" + univId).Result;
                if (response.IsSuccessStatusCode)
                {
                    //result = response.Content.ReadAsAsync<Dictionary<int, DtoBirimler>>().Result;
                    var content = response.Content.ReadAsStringAsync().Result;
                    try
                    {
                        result = JsonConvert.DeserializeObject<Dictionary<int, DtoBirimler>>(content);
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }

            return result;
        }
    }
}
