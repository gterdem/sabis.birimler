﻿using Microsoft.Extensions.DependencyInjection;

namespace Sabis.Birimler.DependencyInjection
{
    public class BirimlerBuilder : IBirimlerBuilder
    {
        public BirimlerBuilder(IServiceCollection services)
        {
            Services = services;
        }

        public IServiceCollection Services { get; }

        public IBirimlerBuilder UseConfig(string serviceUrl, int universityId)
        {
            Services.AddSingleton<IBirimlerService>(c =>
            {
                return new BirimlerService(serviceUrl, universityId);
            });
            return this;
        }

        public IBirimlerBuilder UseSabisConfig()
        {
            Services.AddSingleton<IBirimlerService, BirimlerService>();
            return this;
        }
    }
}
